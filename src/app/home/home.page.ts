import { Component } from '@angular/core';
import { Option } from './options.model';
import {Router} from '@angular/router';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  options: Option[] = [
    {
      imageUrl: "../assets/img/5.png",
      title: 'Η επιχείρηση',
      navLink: ''
    },
    {
      imageUrl: "../assets/img/5.png",
      title: 'Κατηγορίες',
      navLink: '/choose-client/'
    },
    {
      imageUrl: "../assets/img/5.png",
      title: 'Καρτέλα Κίνησης',
      navLink: ''
    },
    {
      imageUrl: "../assets/img/5.png",
      title: 'Αίτημα Ανάληψης',
      navLink: ''
    },
    {
      imageUrl: "../assets/img/5.png",
      title: 'Ενημέρωση Κατάθεσης',
      navLink: ''
    },
  ]

  constructor(
    private router:Router,
  ) { }

  

}
