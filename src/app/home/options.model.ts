export interface Option {
    imageUrl: string;
    title: string;
    navLink: string;
}