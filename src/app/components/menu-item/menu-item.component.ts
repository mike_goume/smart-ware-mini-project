import { Component, Input, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.scss'],
})
export class MenuItemComponent implements OnInit {

  @Input() title:string
  @Input() imageUrl:string
  @Input() navLink:string
  
  constructor(
    private router:Router,
    private toastr: ToastrService
  ) { }

  ngOnInit() {}

  ItemClicked(title) {
    console.log('yey')
    if (title === "Κατηγορίες") {
      this.router.navigate(['choose-client']);
    }else{
      console.log('m')
      this.toastr.error('You can not press this category!', "Major Error" ,{
        positionClass: "toast-bottom-center"
      });
    }
  }

}
