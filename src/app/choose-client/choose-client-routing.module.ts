import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChooseClientPage } from './choose-client.page';

const routes: Routes = [
  {
    path: '',
    component: ChooseClientPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChooseClientPageRoutingModule {}
