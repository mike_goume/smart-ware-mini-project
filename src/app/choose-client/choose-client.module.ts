import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChooseClientPageRoutingModule } from './choose-client-routing.module';

import { ChooseClientPage } from './choose-client.page';
import { ListEntryComponent } from '../components/list-entry/list-entry.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChooseClientPageRoutingModule,
  ],
  declarations: [ChooseClientPage, ListEntryComponent]
})
export class ChooseClientPageModule {}
