export interface Choice {
    description: string;
    name: string;
}