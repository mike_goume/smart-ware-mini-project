import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-choose-client',
  templateUrl: './choose-client.page.html',
  styleUrls: ['./choose-client.page.scss'],
})
export class ChooseClientPage implements OnInit {

  public categories:any=[];
  public shows:any=[];

  constructor(
      private http: HttpClient,
      private router:Router
    ) {
    this.http.get('https://northwind.now.sh/api/categories').subscribe((response) => {
      this.categories=response;
    });
  }

  ngOnInit() {
  }

  ionChange(event) {
    if(event.detail.value === "") {
      this.shows = [];
    }else if(event.detail.value !== "") {
      for(let i in this.categories) {
        if(this.categories[i].name.startsWith(event.detail.value)){
          let count: number = 0;
          for (let j in this.shows) {
            if (this.shows[j].name === this.categories[i].name) {
              count++;
              break;
            }
          }
          if(count === 0) {
            this.shows.push(this.categories[i])
          }
        }
      }
      for(let i in this.shows) {
        if(!this.shows[i].name.startsWith(event.detail.value)) {
          this.shows.splice(i,1);
        }
      }
      console.log(this.shows);  
    }
  }

  ItemClicked() {
    this.router.navigate(['home']);
  }
}
